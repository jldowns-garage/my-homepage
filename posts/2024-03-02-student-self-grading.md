﻿---
title: Giving up control with student self-grading.
created: 2024-03-02
---

I've taught two courses this year: Computer Networks to computer science students, and Human Data Interaction to data science students. In both courses, starting this year, I chose non-standard grading schemes.

- In the Fall, students in Computer Networks built their own portfolios and, with quite a bit of formal structure, assigned themselves midterm and final grades. I didn't give grades for individual assignments but I did give written feedback.
- It's currently Spring, and in Human Data Interaction I'm using a specification grading model [based on Linda Nilson's book](https://www.routledge.com/Specifications-Grading-Restoring-Rigor-Motivating-Students-and-Saving/Nilson/p/book/9781620362426). Each assignment has three rubrics: A-level, B-level, and C-level. Students are assigned grades based on the rubrics they complete. To get an A in the course, students must complete each assignment at the A level. To get a B, they must complete each assignment at a B or above. Revisions and resubmissions are encouraged.

Both of these approaches fall under the umbrella of "ungrading," but they are almost opposites in their approach to "control". In this post I'll talk about implementing self-grading in Computer Networks, wherein I ceded almost all of my control.

The class required students to explain concepts through writing, which has a [high floor and low ceiling](https://www.byrdseed.com/to-differentiate-lower-floors-and-raise-ceilings/). The challenge I encountered was motivating students who were operating near the floor - they would give a few sentences to describe a somewhat complex topic when I was hoping they would provide several paragraphs and perhaps even a few diagrams. These students were certainly capable of providing more detail. The content of the writing assignments would come from their textbook readings, not an advanced analysis or independent research.

It occurred to me that perhaps the lower-performing students *were* putting the same amount of effort into their work; it might just be that they were less skilled than their peers. Luckily I had scheduled "conference weeks" in which I sat down with every student to talk to them about their work, one-on-one. When I talked to the students with shallower portfolios they told me that their biggest challenge was finding time to do the work. The more advanced students usually had strategies to manage their time and ultimately spent more time on the assignments. I concluded that portfolio quality was primarily a function of time spent, not ability.

Since Computer Networks allowed students to grades themselves, many students (and they were honest about this!) completed their assignments tactically - thermodynamics and data structures were less forgiving about late and subpar work. Students struggling in those classes would allocate time away from Computer Networks to focus on the more strict classes. If Computer Networks had a low floor, and students could get credit by operating just below that low floor, they would.

To be clear - a minority of students operated near the "low floor". Most students submitted their portfolios with pride. But I did notice that the disengaged minority grew as the semester went on. Projects from other classes started piling up and the other classes started demanding more time from the students.

I had given the students the power to grade themselves so I couldn't mark them down for insufficient detail; they would have to do that themselves. I found myself having to "convince" the lower-performing students to put more effort into their work.

Early in the semester I tried to encourage them to improve their work by leaving feedback in the form of a question: "How does a router implementing NAT know which host to deliver a datagram to?" I hoped they would revise their portfolio and answer the questions. The top students did; the disengaged students didn't even read my feedback.

Later in the semester my feedback started including phrases like "Does not meet standards." I made sure the students knew that such feedback meant that revisions were required. They had fallen below the low floor.

I felt guilty about this! I adopted student self-grading so that students could set their own standards. With the "does not meet standards" feedback I had taken back the standards-setting power for myself. In some sense I was also taking away the self-grading power. "Does not meet standards" isn't really feedback at all - it's a failing grade that I use 23 characters to express rather than a single "F".

However, since the students were maintaining and revising their portfolios, the "Does not meet standards" feedback was given as a directive to revise their work. I would always provide more useful feedback and point out where they needed to improve.

I might have hoped that students who didn't meet my standards would eventually calibrate their own standards so their first attempt would be above the low floor. This was usually not the case. Students who failed to meet standards in one week were much more likely to fail to meet standards the following weeks. Again, my conclusion was that portfolio quality was primarily a function of time spent, not ability. Penalty-free revisions incentivized some students to aim low and correct if they missed the mark.

Again, my focus was on a (growing) minority of disengaged students. Without grades to motivate them, they didn't do the work. Perhaps it was my fault - I didn't convince them that writing 500 words on the address resolution protocol was a good use of their time. It might have been more important for me to ask the high-performers why they *did* do the work. Did they actually believe it was a good use of their time? Or were they conditioned by eleven years of school to complete their assignments and do what their teacher asked them to do?

What drives people to do hard things? I assume it's different for everyone. I wonder how I can revise the Computer Networks curriculum next year so that more students are convinced that doing the work is a good use of their time, not just something they do because their instructor told them to.

Later this semester I'll write about how, traumatized by the stress of student self-grading, I kept all the control for myself this Spring and implemented specification grading.