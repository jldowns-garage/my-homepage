# Syncs select pages to the external server

# Build all markdown files
./homepage-build.py

# Copy pages to the staging server
DESTINATION="/var/www/external/cs/downs"
FILES=(
    "index.html"
)
DIRS=(
    "css"
    "posts"
    "imgs"
)

# Delete existing destination directories
for DIR in "${DIRS[@]}"; do
    rm -rf "$DESTINATION/$DIR"
done

# Copy local files
for FILE in "${FILES[@]}"; do
    cp -r "$FILE" "$DESTINATION/$FILE"
done

# Copy local directories to the destination
for DIR in "${DIRS[@]}"; do
    cp -r "$DIR" "$DESTINATION/$DIR"
done

# Precondition files for release
cd /var/www/external/cs/downs/

# Fix permissions
find . -type d -exec chmod 755 {} \;
find . -type f -exec chmod 644 {} \;

# Initiate a copy to the public webserver
sync-external