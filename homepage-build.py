#!/usr/bin/python3
import subprocess
import glob
import os

search_dirs = ["./posts"]

# Use glob to find all Markdown files recursively from the search directories
md_files = []
for search_dir in search_dirs:
    md_files.extend(glob.glob(search_dir + '/**/*.md', recursive=True))

print("Found files:")
for md_file_path in md_files:
    print(md_file_path)

for md_file_path in md_files:
    # Construct the path to the template
    template_dir = os.path.join(os.path.dirname(md_file_path), 'template')
    template_path = os.path.join(template_dir, 'template.html')

    # Construct the output HTML file path
    html_file = md_file_path.rsplit('.', 1)[0] + '.html'

    # Check if the template exists
    if os.path.exists(template_path):
        try:
            # Run the pandoc command to convert the Markdown file to HTML using the template
            subprocess.run([
                '/home/scs/downs/.local/bin/pandoc', md_file_path,
                '--template', template_path,
                '-o', html_file],
                check=True
                )
            print(f"Converted {md_file_path} to {html_file} using template {template_path}")
        except subprocess.CalledProcessError as e:
            print(f"Error converting {md_file_path} to HTML: {e}")
    else:
        print(f"Template directory or template.html not found for {md_file_path}")
